/**
 * Primary file for API
 */

 // Dependency 

const http = require('http')
const url = require('url')
const StringDecoder = require('string_decoder').StringDecoder


// The server should respond to all requests with a string
var server = http.createServer(function(req, res){

  // console.log(req)

  // Get the URL and parse it
  var parsedUrl = url.parse(req.url,true)

  // Get the path from that URL
  var path = parsedUrl.pathname
  var trimmedPath = path.replace(/^\/+|\/+$/g, '')

  // Get the query string as an object 
  var queryStringObject = parsedUrl.query

  // Get the HTTP Method
  var method = req.method.toLocaleLowerCase()
  
  // Get the Headers as an object
  var headers = req.headers

  // Get the payload if any 

  //Create decoder object that we will use later on
  var decoder = new StringDecoder('utf-8')
  // declare buffer - placeholder for a string
  var buffer = ''
  // as request is comming in, payload gets streemed in 
  // request object is going to emit data event that we are "bindig on"
  // request on this data object, we use decoder to convert it into simple string 
  req.on('data', function(data){
    buffer += decoder.write(data)
  })
  // on request ending we cup off the buffer with whatever it has ended 
  req.on('end', function(){
    buffer += decoder.end()

    // Send the response
    res.end("hello world \n")

    // Log the request 
    console.log('Request recieved with these payload: ', buffer)

  })
})

// Start server and have it listen on port 3000
server.listen(3000,function(){
  console.log("The server is listening on port 3000 now")
})